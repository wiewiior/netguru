# frozen_string_literal: true

class TopCommentersService
  def call(from_date: 7.days.ago.beginning_of_day, limit: 10)
    User.top_commenters(from_date, limit)
  end
end
