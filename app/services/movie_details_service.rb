# frozen_string_literal: true

class MovieDetailsService
  def initialize(title)
    @title = title.gsub(' ', '%20')
    @base_path = 'https://pairguru-api.herokuapp.com/api/v1/movies/'
    @uri = URI("#{@base_path}#{@title}")
  end

  attr_reader :base_path, :title, :uri

  def call
    Rails.cache.fetch([title, base_path], expires: 24.hour) do
      response = Net::HTTP.get(uri)
      JSON.parse(response)
    end
  end
end
