# frozen_string_literal: true

class MovieExporterNotificationJob < ActiveJob::Base
  queue_as :high_priority
  attr_reader :file_path, :user

  def perform(file_path, user)
    @file_path = file_path
    @user = user
    MovieExporter.new.call(user, file_path)
  end
end
