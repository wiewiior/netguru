# frozen_string_literal: true

class MovieNotificationJob < ActiveJob::Base
  queue_as :high_priority
  attr_reader :movie, :user

  def perform(movie, user)
    @movie = movie
    @user = user
    MovieInfoMailer.send_info(user, movie).deliver_now
  end
end
