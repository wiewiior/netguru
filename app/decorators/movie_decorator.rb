class MovieDecorator < Draper::Decorator
  delegate_all

  def cover
    "https://pairguru-api.herokuapp.com/#{poster_path}"
  end

  def poster_path
    movie_details.dig('data', 'attributes', 'poster')
  end

  def plot
    movie_details.dig('data', 'attributes', 'plot')
  end

  def rating
    movie_details.dig('data', 'attributes', 'rating')
  end

  private

  def movie_details
    MovieDetailsService.new(title).call
  end
end
