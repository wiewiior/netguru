# frozen_string_literal: true

class TitleBracketsValidator < ActiveModel::Validator
  def validate(record)
    return if validate_string(record.title)

    record.errors.add(:base, 'Invalid')
  end

  private

  def any_empty_brackets?(title)
    %w[() [] {}].none? { |bracket| title.include?(bracket) }
  end

  def validate_string(title)
    return false unless any_empty_brackets?(title)
    brackets_hash = { '(' => ')', '{' => '}', '[' => ']' }
    brackets = []
    title.each_char do |x|
      if brackets_hash.keys.include?(x)
        brackets.push(x)
      elsif brackets_hash.values.include?(x)
        return false if brackets_hash.key(x) != brackets.pop
      end
    end
    brackets.empty?
  end
end
