class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  validates :comment_text, presence: true
  validates :user_id,
            uniqueness: {
              scope: :movie_id,
              message: 'already commented'
            }
end
