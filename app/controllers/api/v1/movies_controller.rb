# frozen_string_literal: true

module Api
  module V1
    class MoviesController < Api::BaseController
      before_action :check_movie, only: :show
      def index
        render json: Movie.all, each_serializer: MovieIndexSerializer
      end

      def show
        render json: movie, serializer: MovieShowSerializer
      end

      private

      def check_movie
        return if movie

        render json: { message: 'No such movie' }, status: :not_found
      end

      def movie
        Movie.all.find_by(id: params[:id])
      end
    end
  end
end
