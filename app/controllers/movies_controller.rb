class MoviesController < ApplicationController
  before_action :authenticate_user!, only: [:send_info]

  def index
    @movies = Movie.decorate
  end

  def show
    @movie = movie.decorate
  end

  def send_info
    MovieNotificationJob.perform_later(movie, current_user)
    redirect_back(fallback_location: root_path, notice: 'Email sent with movie info')
  end

  def export
    MovieExporterNotificationJob.perform_later('tmp/movies.csv', current_user)
    redirect_to root_path, notice: 'Movies exported'
  end

  private

  def movie
    Movie.find(params[:id])
  end
end
