class CommentsController < ApplicationController
  before_action :check_movie, only: %i[create destroy]
  before_action :check_comment, only: %i[destroy]
  before_action :authenticate_user!, only: %i[create destroy]

  def create
    @comment = movie.comments.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      redirect_to movie_path(movie), notice: 'Comment successfully added'
    else
      errors = @comment.errors.messages.values.flatten.join(', ')
      redirect_to movie_path(movie),
                  alert: "There was a problem with your comment: #{errors}"
    end
  end

  def destroy
    if comment.destroy
      redirect_to movie_path(movie), notice: 'Comment successfully deleted'
    else
      errors = comment.errors.messages.values.flatten.join(', ')
      redirect_to movie_path(movie),
                  alert: "There was a problem with deleting comment: #{errors}"
    end
  end

  def top_commenters
    @users = TopCommentersService.new.call
  end

  private

  def comment
    movie.comments.find_by(id: params[:id], user_id: current_user.id)
  end

  def comment_params
    params.require(:comment).permit(:comment_text)
  end

  def check_comment
    return true if comment

    redirect_to root_path, alert: 'Not found comment'
  end

  def check_movie
    return true if movie

    redirect_to root_path, alert: 'Not found movie'
  end

  def movie
    Movie.find_by(id: params[:movie_id])
  end
end
