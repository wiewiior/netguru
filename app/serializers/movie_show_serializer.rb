# frozen_string_literal: true

class MovieShowSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :genre_id, :genre_name

  def genre_name
    object.genre.name
  end
end
