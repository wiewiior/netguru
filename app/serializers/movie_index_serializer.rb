# frozen_string_literal: true

class MovieIndexSerializer < ActiveModel::Serializer
  attributes :id, :title
end
