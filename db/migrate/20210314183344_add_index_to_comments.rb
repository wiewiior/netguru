class AddIndexToComments < ActiveRecord::Migration[5.2]
  def change
    add_index :comments, %i[movie_id user_id], unique: true
  end
end
