require "rails_helper"

describe Comment do
  before do
    Movie.destroy_all
    Comment.destroy_all
    User.destroy_all
    @user = create(:user)
    @movie = create(:movie)
  end

  let(:user) { User.find_by(id: @user.id) }
  let(:movie) { Movie.find_by(id: @movie.id) }
  let(:user_id) { user.id }
  let(:comment_text) { Faker::Lorem.paragraph }
  let(:comment) do
    movie.comments.new(user_id: user_id, comment_text: comment_text)
  end

  context 'correct parameters' do
    it { expect(comment.validate).to be_truthy }
  end

  context 'invalid parameters' do
    context 'empty comment' do
      let(:comment_text) { '' }

      it { expect(comment.validate).to be_falsey }

      it 'return message' do
        comment.validate
        expect(comment.errors.full_messages)
          .to eq ["Comment text can't be blank"]
      end
    end

    context 'already commented' do
      before { create(:comment, movie_id: movie.id, user_id: user_id) }

      it { expect(comment.validate).to be_falsey }

      it 'return message' do
        comment.validate
        expect(comment.errors.full_messages).to eq ["User already commented"]
      end
    end

    context 'without user' do
      let(:user_id) { nil }

      it { expect(comment.validate).to be_falsey }

      it 'return message' do
        comment.validate
        expect(comment.errors.full_messages).to eq ["User must exist"]
      end
    end
  end
end