# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MovieDetailsService, type: :service do
  subject :api_response do
    described_class.new(title).call
  end

  let(:title) { 'Deadpool' }

  ['Deadpool', 'Godfather', 'Pulp Fiction', 'Star Wars V'].each do |title|
    context "return correct title for: #{title}" do
      let(:title) { title }

      it { expect(api_response).to be_kind_of(Hash) }
      it {
        expect(api_response.dig('data', 'attributes', 'title')).to eq title
      }
    end
  end

  context 'return correct keys' do
    it { expect(api_response).to have_key('data') }
  end

  context 'return correct keys for date' do
    %w[id type attributes].each do |key|
      it key.to_s do
        expect(api_response['data']).to have_key(key)
      end
    end
  end

  context 'return correct keys for attributes' do
    %w[title plot rating poster].each do |key|
      it key.to_s do
        expect(api_response.dig('data', 'attributes')).to have_key(key)
      end
    end
  end

  context 'invalid output' do
    context 'return error if no details' do
      let(:title) { Faker::Lorem.word }

      it { expect(api_response['message']).to eq "Couldn't find Movie" }
    end
  end
end
