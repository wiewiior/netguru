# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TopCommentersService, type: :service do
  before :all do
    User.destroy_all
    Comment.destroy_all
    Movie.destroy_all
    @user1 = create(:user)
    @comment = create(:comment, user_id: @user1.id, created_at: 10.days.ago)
    @user2 = create(:user)
    @comment = create(:comment, user_id: @user2.id)
    @comment = create(:comment, user_id: @user2.id)
    @comment = create(:comment, user_id: @user2.id)
    @comment = create(:comment, user_id: @user2.id)
    @comment = create(:comment, user_id: @user2.id)
    @user3 = create(:user)
    @comment = create(:comment, user_id: @user3.id, created_at: 6.days.ago)
    @comment = create(:comment, user_id: @user3.id, created_at: 3.days.ago)
    @comment = create(:comment, user_id: @user3.id, created_at: 3.days.ago)
    @user4 = create(:user)
    @comment = create(:comment, user_id: @user4.id, created_at: 5.days.ago)
    @comment = create(:comment, user_id: @user4.id, created_at: 1.days.ago)
  end

  subject { described_class.new.call(from_date: from_date, limit: limit) }

  context 'return correct users' do
    context '7 days ago' do
      let(:from_date) { 7.days.ago }
      let(:limit) { 10 }
      let(:correct_order) { [@user2.id, @user3.id, @user4.id] }

      it { expect(subject.length).to eq 3 }
      it { expect(subject.map(&:id)).to eq correct_order }

      context 'limit 2' do
        let(:limit) { 2 }
        let(:correct_order) { [@user2.id, @user3.id] }

        it { expect(subject.length).to eq 2 }
        it { expect(subject.map(&:id)).to eq correct_order }
      end
    end

    context '12 days ago' do
      let(:from_date) { 12.days.ago }
      let(:limit) { 10 }
      let(:correct_order) { [@user2.id, @user3.id, @user4.id, @user1.id] }

      it { expect(subject.length).to eq 4 }
      it { expect(subject.map(&:id)).to eq correct_order }

      context 'limit 2' do
        let(:limit) { 2 }
        let(:correct_order) { [@user2.id, @user3.id] }

        it { expect(subject.length).to eq 2 }
        it { expect(subject.map(&:id)).to eq correct_order }
      end
    end
  end
end
