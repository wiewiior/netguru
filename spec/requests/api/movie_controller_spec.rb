# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::MoviesController, type: :request do
  before :all do
    Movie.destroy_all
    @title1 = Faker::Lorem.word
    @title2 = Faker::Lorem.word
    @title3 = Faker::Lorem.word
    @title4 = Faker::Lorem.word
    @movie1 = create(:movie, title: @title1)
    create(:movie, title: @title2)
    create(:movie, title: @title3)
    create(:movie, title: @title4)
  end

  let(:response_body) { JSON.parse(response.body) }
  let(:movie1) { Movie.find_by(id: @movie1.id) }
  let(:titles) { [@title1, @title2, @title3, @title4] }

  describe 'GET api/movies' do
    subject { get '/api/movies/' }

    before { subject }

    it 'return status :ok' do
      expect(response.status).to eq 200
    end

    %w[id title].each do |key|
      it "return correct key #{key}" do
        expect(response_body[0].keys).to include key
      end
    end

    it 'return correct number of movies' do
      expect(response_body.count).to eq 4
    end

    it 'return correct movie titles' do
      expect(response_body.map { |movie| movie['title'] }).to match_array titles
    end
  end

  describe 'GET api/movies/:id' do
    subject { get "/api/movies/#{movie_id}" }

    before { subject }

    context 'correct id' do
      let(:movie_id) { movie1.id }

      it 'return status :ok' do
        expect(response.status).to eq 200
      end

      %w[id title description genre_id genre_name].each do |key|
        it "return correct key #{key}" do
          expect(response_body.keys).to include key
        end
      end

      it 'return correct movie title' do
        expect(response_body.dig('title')).to eq movie1.title
      end

      it 'return correct movie genre_id' do
        expect(response_body.dig('genre_id')).to eq movie1.genre_id
      end
    end

    context 'invalid id' do
      let(:movie_id) { movie1.id + 1000 }

      it 'return status :not_found' do
        expect(response.status).to eq 404
      end

      it 'return error message' do
        expect(response_body['message']).to eq 'No such movie'
      end
    end
  end
end
