# frozen_string_literal: true

require 'rails_helper'
RSpec.describe MovieExporterNotificationJob, type: :job do
  before :all do
    @user = create(:user)
    @file_path = 'tmp/movies.csv'
  end

  let(:user) { User.find_by(id: @user.id) }
  let(:file_path) { @file_path }

  subject { described_class.perform_now(file_path, user) }

  it 'works' do
    allow(described_class).to receive(:perform_later).and_return(true)
    subject
  end

  it 'sends email' do
    perform_enqueued_jobs do
      expect { subject }.to(change { ApplicationMailer.deliveries.count }.by(1))
    end
  end
end
