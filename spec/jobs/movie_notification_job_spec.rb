# frozen_string_literal: true

require 'rails_helper'
RSpec.describe MovieNotificationJob, type: :job do
  before :all do
    Movie.destroy_all
    @user = create(:user)
    @movie = create(:movie)
  end

  let(:user) { User.find_by(id: @user.id) }
  let(:movie) { Movie.find_by(id: @movie.id) }

  subject { described_class.perform_now(movie, user) }

  it 'works' do
    allow(described_class).to receive(:perform_later).and_return(true)
    subject
  end

  it 'sends email' do
    perform_enqueued_jobs do
      expect { subject }.to(change { ApplicationMailer.deliveries.count }.by(1))
    end
  end
end
