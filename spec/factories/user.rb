FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password { SecureRandom.hex(8) }
    confirmed_at { Time.zone.now }
  end
end
