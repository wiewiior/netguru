FactoryBot.define do
  factory :comment do
    movie
    user
    comment_text { Faker::Lorem.paragraph }
    created_at { Time.zone.now }
    updated_at { created_at }
  end
end
