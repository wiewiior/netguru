# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  before :all do
    @user = create(:user)
    @movie = create(:movie)
  end

  let(:movie) { Movie.find_by(id: @movie.id) }
  let(:movie_id) { movie.id }
  let(:user) { User.find_by(id: @user.id) }

  before { sign_in user }

  context 'create' do
    let(:create_params) do
      { movie_id: movie.id, comment: { comment_text: comment_text } }
    end

    subject { post :create, params: create_params }

    context 'valid parameters' do
      let(:comment_text) { Faker::Lorem.paragraph }

      it { expect { subject }.to change(Comment, :count).by(1) }
      it { expect { subject }.to change { movie.comments.count }.by(1) }

      context 'after save' do
        before { subject }

        it { expect(flash[:notice]).to be_present }
        it { expect(flash.notice).to include 'Comment successfully added' }
        it { expect(response).to redirect_to(movie_path(movie)) }
      end
    end

    context 'invalid parameters' do
      let(:comment_text) { ' ' }

      it { expect { subject }.to_not change(Comment, :count) }
      it { expect { subject }.to_not(change { movie.comments.count }) }

      context 'return error' do
        before { subject }

        it { expect(response).to redirect_to(movie_path(movie)) }
        it { expect(flash[:alert]).to be_present }
        it {
          expect(flash.alert).to include 'There was a problem with your comment'
        }
      end
    end
  end

  context 'destroy' do
    before do
      @comment = create(:comment, user_id: user.id, movie_id: movie.id)
      user2 = create(:user)
      @comment2 = create(:comment, user_id: user2.id, movie_id: movie.id)
    end

    let(:comment) { Comment.find_by(id: @comment.id) }
    let(:comment2) { Comment.find_by(id: @comment2.id) }

    subject { post :destroy, params: { movie_id: movie_id, id: comment_id } }

    context 'correct comment' do
      let(:comment_id) { comment.id }

      it { expect { subject }.to change(Comment, :count).by(-1) }
      it { expect { subject }.to change { movie.comments.count }.by(-1) }

      context 'after delete' do
        before { subject }

        it { expect(response).to redirect_to(movie_path(movie)) }
        it { expect(flash[:notice]).to be_present }
        it { expect(flash.notice).to include 'Comment successfully deleted' }
      end

      context 'return error' do
        before do
          allow_any_instance_of(Comment).to receive(:destroy).and_return(false)
        end

        it { expect { subject }.to_not change(Comment, :count) }
        it { expect { subject }.to_not(change { movie.comments.count }) }
      end
    end

    context 'comment not exist' do
      let(:comment_id) { comment.id + 1000 }

      it { expect { subject }.to_not change(Comment, :count) }
      it { expect { subject }.to_not(change { movie.comments.count }) }

      context 'return error' do
        before { subject }

        it { expect(response).to redirect_to(root_path) }
        it { expect(flash[:alert]).to be_present }
        it { expect(flash.alert).to include 'Not found comment' }
      end
    end

    context 'comment do not belong to user' do
      let(:comment_id) { comment2.id }

      it { expect { subject }.to_not change(Comment, :count) }
      it { expect { subject }.to_not(change { movie.comments.count }) }

      context 'return error' do
        before { subject }

        it { expect(response).to redirect_to(root_path) }
        it { expect(flash[:alert]).to be_present }
        it { expect(flash.alert).to include 'Not found comment' }
      end
    end

    context 'movie not exist' do
      let(:movie_id) { movie.id + 999 }
      let(:comment_id) { comment.id }

      it { expect { subject }.to_not change(Comment, :count) }
      it { expect { subject }.to_not(change { movie.comments.count }) }

      context 'return error' do
        before { subject }

        it { expect(response).to redirect_to(root_path) }
        it { expect(flash[:alert]).to be_present }
        it { expect(flash.alert).to include 'Not found movie' }
      end
    end
  end

  context 'top_commenters' do
    subject { get :top_commenters }

    it do
      subject
      expect(response).to have_http_status 200
    end
  end
end
