Rails.application.routes.draw do
  devise_for :users

  namespace :api do
    scope module: :v1, defaults: { format: :json } do
      resources :movies, only: %i[index show]
    end
  end

  root "home#welcome"
  resources :genres, only: :index do
    member do
      get "movies"
    end
  end
  resources :movies, only: [:index, :show] do
    member { get :send_info }
    collection { get :export }
    resources :comments, only: %i[create destroy]
  end
  get '/top_commenters' => 'comments#top_commenters'
end
